#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "500 600\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  500)
        self.assertEqual(j, 600)

    def test_read_3(self):
        s = "55 904\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  55)
        self.assertEqual(j, 904)

    def test_read_4(self):
        s = "15000 16000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  15000)
        self.assertEqual(j, 16000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(1, 10000)
        self.assertEqual(v, 262)

    def test_eval_7(self):
        v = collatz_eval(1, 5000)
        self.assertEqual(v, 238)

    def test_eval_8(self):
        v = collatz_eval(5000, 10000)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 500, 600, 800)
        self.assertEqual(w.getvalue(), "500 600 800\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 10000, 11000, 12000)
        self.assertEqual(w.getvalue(), "10000 11000 12000\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 100000, 200000, 300000)
        self.assertEqual(w.getvalue(), "100000 200000 300000\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n1 1\n1 10000\n1 5000\n5000 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 1 1\n1 10000 262\n1 5000 238\n5000 10000 262\n")

    def test_solve_2(self):
        r = StringIO("1 100\n100 200\n200 300\n300 400\n400 500\n500 600\n600 700\n700 800\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 100 119\n100 200 125\n200 300 128\n300 400 144\n400 500 142\n500 600 137\n600 700 145\n700 800 171\n")

    def test_solve_3(self):
        r = StringIO("7100 7200\n7200 7300\n7300 7400\n7400 7500\n7500 7600\n7600 7700\n7700 7800\n7800 7900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "7100 7200 195\n7200 7300 177\n7300 7400 208\n7400 7500 239\n7500 7600 208\n7600 7700 177\n7700 7800 221\n7800 7900 190\n")

    def test_solve_4(self):
        r = StringIO("123 560\n55 904\n2343 7685\n9594 10030\n10067 15089\n4500 9012\n2348 5678\n80234 97054\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "123 560 144\n55 904 179\n2343 7685 262\n9594 10030 242\n10067 15089 276\n4500 9012 262\n2348 5678 238\n80234 97054 333\n")

    def test_solve_5(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n1 1\n10000 1\n5000 1\n10000 5000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n1 1 1\n10000 1 262\n5000 1 238\n10000 5000 262\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
